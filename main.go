/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/filip.boye.kofi/puppetilizer/cmd"

func main() {
	cmd.Execute()
}
