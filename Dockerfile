FROM golang:1.18 AS builder
WORKDIR /puppetilizer
COPY . .
COPY go.mod .
COPY go.sum .
ENV CGO_ENABLED=0 
RUN go build -a -installsuffix cgo -o app .

FROM alpine:3 AS environment
RUN apk --no-cache add ca-certificates
WORKDIR /main
COPY --from=builder /puppetilizer/app .
CMD ["./app"]